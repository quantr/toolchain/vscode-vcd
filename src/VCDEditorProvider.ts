import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';

export class VCDEditorProvider implements vscode.CustomTextEditorProvider {

	public static register(context: vscode.ExtensionContext): vscode.Disposable {
		const provider = new VCDEditorProvider(context);
		let s: vscode.WebviewPanelOptions = {
			retainContextWhenHidden: true
		};
		const providerRegistration = vscode.window.registerCustomEditorProvider(VCDEditorProvider.viewType, provider, { webviewOptions: s });
		return providerRegistration;
	}

	private static readonly viewType = 'quantr.vcd';

	constructor(
		private readonly context: vscode.ExtensionContext
	) { }

	public async resolveCustomTextEditor(
		document: vscode.TextDocument,
		webviewPanel: vscode.WebviewPanel,
		_token: vscode.CancellationToken
	): Promise<void> {
		// Setup initial content for the webview
		webviewPanel.webview.options = {
			enableScripts: true
		};
		// webviewPanel.webview.html = this.getHtmlForWebview(webviewPanel.webview);
		webviewPanel.webview.html = this.loadWebView(webviewPanel, this.context.extensionPath);

		function updateWebview(context: vscode.ExtensionContext) {
			var cp = require('child_process');
			const folder = path.join(context.extensionPath, 'jar');
			console.log('updateWebview ' + "java -jar " + folder + "/quantr-vcd-library-1.0.jar " + document.uri.path);
			let child = cp.exec("java -jar " + folder + "/quantr-vcd-library-1.0.jar " + document.uri.path);
			var fileContent = '';
			child.stdout.on('data', (data: any) => {
				fileContent += data;
			});
			child.on('exit', (code: any) => {
				webviewPanel.webview.postMessage({
					type: 'update',
					text: fileContent,
				});
			});
		}

		// Hook up event handlers so that we can synchronize the webview with the text document.
		//
		// The text document acts as our model, so we have to sync change in the document to our
		// editor and sync changes in the editor back to the document.
		// 
		// Remember that a single text document can also be shared between multiple custom
		// editors (this happens for example when you split a custom editor)

		const changeDocumentSubscription = vscode.workspace.onDidChangeTextDocument(e => {
			if (e.document.uri.toString() === document.uri.toString()) {
				updateWebview(this.context);
			}
		});

		// Make sure we get rid of the listener when our editor is closed.
		webviewPanel.onDidDispose(() => {
			changeDocumentSubscription.dispose();
		});

		// Receive message from the webview.
		webviewPanel.webview.onDidReceiveMessage(e => {
			switch (e.type) {
				case 'add':
					return;
				case 'delete':
					return;
			}
		});

		// webviewPanel.onDidChangeViewState(() => {
		// 	updateWebview(this.context);
		// });

		updateWebview(this.context);
	}

	private loadWebView(panel: vscode.WebviewPanel, extensionPath: string) {
		const resourcePath = path.join(extensionPath, 'src', 'webview', 'index.html');
		let html = fs.readFileSync(resourcePath, 'utf-8');

		const bootstrapSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview', 'bootstrap-4.5.2-dist', 'css', 'bootstrap.css')));
		const basePathSrc = panel.webview.asWebviewUri(vscode.Uri.file(path.join(extensionPath, 'src', 'webview')));
		html = html.replace(/{basePath}/g, basePathSrc.toString());
		return html;
	}
}
