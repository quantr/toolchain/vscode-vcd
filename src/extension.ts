import * as vscode from 'vscode';
import { VCDEditorProvider } from './VCDEditorProvider';

export function activate(context: vscode.ExtensionContext) {
	let disposable = vscode.commands.registerCommand('vscode-vcd.helloWorld', () => {
		vscode.window.showInformationMessage('Hello World from VSCode VCD!');
	});

	context.subscriptions.push(disposable);
	context.subscriptions.push(VCDEditorProvider.register(context));
}

export function deactivate() {}

